import React from "react";
import { getImages } from "../clientApi";

class ImageList extends React.Component {
  state = {
    images: []
  };

  componentDidMount() {
    getImages().then(getImagesResponse => {
      // debugger;
      console.log(getImagesResponse);
      this.setState((state, props) => {
        return { images: getImagesResponse.imageURI };
      });
    });
  }

  render() {
    return (
      <>
        {this.state.images.map(imageURL => {
          console.log(imageURL);
          return <img src={imageURL} />;
        })}
      </>
    );
  }
}

export default ImageList;
